add_subdirectory(desktoptheme)
add_subdirectory(icons)
add_subdirectory(translations)

if(KUserFeedback_FOUND)
    add_subdirectory(feedback)
endif()
